<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class HomeSection extends Model implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'num',
              $table      = 'homepage_sections',
              $connection = 'krehub';

}
