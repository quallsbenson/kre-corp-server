<?php


use App\Repositories\HomeSectionRepositoryEloquent as HomeSections;
use App\Repositories\PropertyRepositoryEloquent as Property;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group([ 'prefix' => '/api/hub', 'middleware' => ['cors'] ], function(){

	/**
	* KRE Hub Home page data
	**/

	Route::get('/home', function( HomeSections $homeSection ){

		return $homeSection->getAll();
	});

	/**
	* Get Property Data
	**/

	Route::get('/properties', function( Property $property ){

		return $property->all();
	});
});
