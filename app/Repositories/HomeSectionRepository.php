<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface HomeSectionRepository
 * @package namespace App\Repositories;
 */
interface HomeSectionRepository extends RepositoryInterface
{
    //
}
