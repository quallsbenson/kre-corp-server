<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\HomeSectionRepository;
use App\Entities\HomeSection;
use App\Validators\HomeSectionValidator;

/**
 * Class HomeSectionRepositoryEloquent
 * @package namespace App\Repositories;
 */
class HomeSectionRepositoryEloquent extends BaseRepository implements HomeSectionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return HomeSection::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
    * Load all sections in order
    * @return Collection
    **/

    public function getAll()
    {
        return $this->model
            ->orderBy( 'dragSortOrder', 'DESC' )
            ->get();
    }
}
